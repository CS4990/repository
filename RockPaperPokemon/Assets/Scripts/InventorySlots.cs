﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySlots : MonoBehaviour
{
    private PlayerController playerInventory;


    private void Start()
    {
        playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    public void DropItemZero()
    {
        foreach (Transform child in transform)
        {
            //child.GetComponent<SpawnItem>().SpawnDropped();
            GameObject.Destroy(child.gameObject);
            playerInventory.isFull[0] = false;

        }
    }

    public void DropItemOne()
    {
        foreach (Transform child in transform)
        {
            //child.GetComponent<SpawnItem>().SpawnDropped();
            GameObject.Destroy(child.gameObject);
            playerInventory.isFull[1] = false;
        }
    }

    public void DropItemTwo()
    {
        foreach (Transform child in transform)
        {
            //child.GetComponent<SpawnItem>().SpawnDropped();
            GameObject.Destroy(child.gameObject);
            playerInventory.isFull[2] = false;
        }
    }

}
