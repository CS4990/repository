using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SFX : MonoBehaviour { // Individual audio object for pool

    public AudioSource aud;

    float startTime;
    float startVol = 0f;
    float endVol = 1f;
    float duration = -1f;

    public bool panning { get; private set; } // 3D sound effects
    Transform focus;
    float range;

    bool active;

    // public getters
    public float StartTime { get { return startTime; } }
    public float StartVol { get { return startVol; } }
    public float EndVol { get { return endVol; } }
    public float Duration { get { return duration; } }

    Coroutine fade;

	void Awake () {

        DontDestroyOnLoad(this);

        aud = GetComponent<AudioSource>(); // Set references
        aud.playOnAwake = false;
        active = false;
        fade = null;
	}

    // First function call. Sets all values
    public void Set(AudioClip clip, float volume = 1f, bool loop = false, float pitch = 1f)
    {
        aud.clip = clip;
        aud.volume = volume;
        aud.pitch = pitch;
        aud.loop = loop;
        
        startVol = 0f;
        endVol = volume;
    }

    // Override for Set (No loop)
    public void Set(AudioClip clip, float volume = 1f, float pitch = 1f)
    {
        Set(clip, volume, false, pitch);
    }

    public void SetFade(float start, float end, float t, float pitch = 1f)
    {
        startVol = start;
        endVol = end;
        duration = t;
        startTime = Time.unscaledTime;
        aud.pitch = pitch;

        StopAllCoroutines();
        fade = null;
    }

    // Enabled panning audio and sets focal point
    public void SetPanning(Transform target, float r)
    {
        panning = true;
        focus = target;
        range = r;
    }

    // Sets mixer to "Effects" (true) or "Clean" (false)
    public void SetMixer(bool effects, bool voice = false)
    {
        if (voice)
        {
            aud.outputAudioMixerGroup = AudioPool.Instance.voiceEffects;
            return;
        }

        if (effects) aud.outputAudioMixerGroup = AudioPool.Instance.effects;
        else aud.outputAudioMixerGroup = AudioPool.Instance.clean;
    }

    // Activate the object
    public void Activate()
    {
        if (!aud.isPlaying) aud.Play();
        if (duration != -1f) fade = StartCoroutine(FadeLoop());

        if (!panning) aud.panStereo = 0f;

        active = true;
    }

    // Deactivate the object
    public void Deactivate()
    {
        aud.Stop();
        aud.outputAudioMixerGroup = null;
        panning = false;
        active = false;
        duration = -1;

        startVol = 0f;
        endVol = 1f;
    }

    void Update() // Checks for end of clip, then deactivates
    {
        if (!active) return;

        if (panning)
        {
            if (!focus || !Camera.main)
            {
                AudioPool.Reset(this);
                return;
            }
        }

        if (null == fade)
        {
            if (!aud.isPlaying && (!aud.loop || aud.volume == 0))
                AudioPool.Reset(this);
        }
    }

    IEnumerator FadeLoop()
    {
        while (Time.unscaledTime - startTime < duration)
        {
            aud.volume = startVol + ((Time.unscaledTime - startTime) / duration) * (endVol - startVol);
            yield return null;
        }

        aud.volume = endVol;
        duration = -1; // Used to check if this SFX should be fading sound
        if (endVol == 0) aud.Stop();

        fade = null;
        yield break;
    }

    public void UpdateVolume(float oldV, float newV)
    {
        oldV = Mathf.Max(0.001f, oldV);
        newV = Mathf.Max(0.001f, newV);

        if (aud.loop)
        {
            startVol = (startVol / oldV * newV);
            endVol = (endVol / oldV * newV);
        }

        aud.volume = (aud.volume / oldV * newV);
    }
}