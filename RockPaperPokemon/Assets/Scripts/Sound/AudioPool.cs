using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioPool : MonoBehaviour {

    const string MUSIC = "music";

    // Singleton instantiation
    private static AudioPool instance;
    public static AudioPool Instance
    {
        get
        {
            if (instance == null) 
            {
                instance = GameObject.FindObjectOfType<AudioPool>(); 
                instance.FillPool();
            }
            return instance;
        }
    }

    List<SFX> availablePool = new List<SFX>();
    List<SFX> usedPool = new List<SFX>();

    Dictionary<string, SFX> activeLoops = new Dictionary<string, SFX>();
    Dictionary<string, float> pausedVols = new Dictionary<string, float>();

    public AudioMixer master;
    public AudioMixerGroup clean;
    public AudioMixerGroup effects;
    public AudioMixerGroup voiceEffects;

    private static float p_effectsVolume = 1f;
    private static float p_musicVolume = 1f;
    private float p_masterVolume = 1f;

    public static float effectsVolume
    {
        get { return p_effectsVolume; }
        set
        {
            value = Mathf.Clamp(value, 0.001f, 1f);
            if (value != p_effectsVolume)
                UpdateVolume(false, p_effectsVolume, value);

            p_effectsVolume = value;
        }
    }

    public static float musicVolume
    {
        get { return p_musicVolume; }
        set
        {
            value = Mathf.Clamp(value, 0.001f, 1f);
            if (value != p_musicVolume)
                UpdateVolume(true, p_musicVolume, value);

            p_musicVolume = value;
        }
    }

    // Used for fade in/out
    public static float masterVolume
    {
        get { return Instance.p_masterVolume; }
        set
        {
            value = Mathf.Clamp(value, 0.001f, 1f);
            if (value != Instance.p_masterVolume)
                UpdateMasterVolume(Instance.p_masterVolume, value);
                //UpdateVolume(false, Instance.p_masterVolume * effectsVolume, value * effectsVolume);

            Instance.p_masterVolume = value;
        }
    }

    // Used for pause
    static float pausedEffectsVolume;
    static float pausedMusicVolume;

    public delegate void VolumeDelegate();
    public static VolumeDelegate onVolumeChange; 

    static int poolAmount = 50;

    public static bool stopNewSounds = false;

    void Start()
    {
        if (Instance != this) Destroy(this.gameObject);
        else DontDestroyOnLoad(this.gameObject);
    }

    // Populate SFX object pool with <poolAmount>
    void FillPool()
    {
        for (int i = 0; i < poolAmount; i++)
        {
            SFX newSound = new GameObject("sfx" + i.ToString(), typeof(SFX)).GetComponent<SFX>();
            newSound.transform.SetParent(transform);
            newSound.gameObject.SetActive(false);

            availablePool.Add(newSound);
        }
    }

    public static void DimLoops(float volume)
    {
        pausedEffectsVolume = effectsVolume;
        pausedMusicVolume = musicVolume;

        Instance.pausedVols.Clear();
        List<string> sound3ds = new List<string>();

        foreach (KeyValuePair<string, SFX> p in Instance.activeLoops)
        {
            if (p.Value.panning)
                sound3ds.Add(p.Key);
            else
            {
                Instance.pausedVols.Add(p.Key, p.Value.EndVol);
                PlayLoop(null, p.Key, p.Value.EndVol, volume, 0.25f);
            }
        }

        for (int i = 0; i < sound3ds.Count; i++)
            StopLoop(sound3ds[i], 0.25f);
    }

    public static void RestoreLoops()
    {
        foreach (KeyValuePair<string, float> p in Instance.pausedVols)
        {
            bool isMusic = p.Key == MUSIC;
            float newVol = isMusic ? (p.Value / pausedMusicVolume) : (p.Value / pausedEffectsVolume);
            PlayLoop(null, p.Key, 0f, newVol, 0.25f);
        }
    }

    // Play a single sound once, with the given settings
    public static void PlayOneShot(AudioClip clip, float volume = 1, float pitch = 1, bool useEffect = true, bool is3D = false, Transform f = null, float r = 0)
    {
        if (stopNewSounds) return;

        SFX p = Instance.GetSFX();
        if (!p) return;

        volume *= effectsVolume * masterVolume;

        // Order of operations for using SFX objects
        p.Set(clip, volume, pitch);
        if (is3D) p.SetPanning(f, r);
        p.SetMixer(useEffect);
        p.Activate();
    }

    // Multi-use function for looping tracks
    // Set <clipName> in order to have future references to the audio source
    // If <clipName> already exists, it will be smoothly replaced
    // <clip> can be left blank to modify the <clipName> SFX, regardless of what AudioClip it is playing
    public static void PlayLoop(AudioClip clip, string clipName, float startVolume, float endVolume, float duration, bool loop = true, float pitch = 1f, bool useEffect = true, bool is3D = false, Transform f = null, float r = 0)
    {
        if (stopNewSounds) return;

        SFX p = null;
        bool exists = false;

        if (clipName.ToLower() == MUSIC)
        {
            startVolume *= musicVolume * masterVolume;
            endVolume *= musicVolume * masterVolume;
        }
        else
        {
            startVolume *= effectsVolume * masterVolume;
            endVolume *= effectsVolume * masterVolume;
        }

        if (Instance.activeLoops.ContainsKey(clipName))
        {
            p = Instance.activeLoops[clipName]; // Set reference if clipName already exists

            if (!p || !p.aud || !p.aud.clip)
            {
                //Debug.LogError("Missing instance in loop dictionary.");
                return;
            }

            if (!clip || p.aud.clip.Equals(clip)) // If !clip, adjust volume of existing audio
            {
                exists = true;
                if (p.aud.volume != startVolume && !p.panning) startVolume = p.aud.volume;
            }
            else
            {
                // Same clipName, different clip playing. Toggles a simultaneous fade in/out, using an additional SFX.
                p.SetFade(p.aud.volume, 0f, duration, pitch);
                p.Activate();
                Instance.activeLoops.Remove(clipName);
                // Removes fading out SFX from dictionary, treat new clip as a new SFX
            }
        }
        
        if (!exists) // If there is no existing clip, or if there is a transition
        {
            p = Instance.GetSFX();
            if (!p) return;
            
            Instance.activeLoops.Add(clipName, p); // Add to dictionary
            if (clip) p.Set(clip, startVolume, loop, pitch); // If !clip, clip and volume do not need to be set
        }

        p.SetFade(startVolume, endVolume, duration, pitch);

        if (is3D) p.SetPanning(f, r);
        p.Activate();
    }

    // Simplified functions to play create quick 3D sound sources
    public static void SimpleOneShot3D(AudioClip clip, float volume, Transform center, float range)
    {
        PlayOneShot(clip, volume, 1f, true, true, center, range);
    }

    public static void SimpleLoop3D(AudioClip clip, string clipName, Transform center, float range)
    {
        PlayLoop(clip, clipName, 0f, 1f, 0f, true, 1f, true, true, center, range);
    }

    // Simple function to stop a loop by name.
    public static void StopLoop(string clipName, float duration = 0f)
    {
        if (!Instance.activeLoops.ContainsKey(clipName)) return;

        AudioPool.PlayLoop(null, clipName, 0f, 0f, duration);
        if (duration == 0f) Instance.activeLoops.Remove(clipName);

        //Debug.Log("Stopped loop " + clipName);
    }

    // Stop all loops with names that contain a string
    public static void StopAllLoops(string contains, float duration = 0f)
    {
        List<string> keys = new List<string>(Instance.activeLoops.Keys);

        for (int i = 0; i < keys.Count; i++)
            if (keys[i].Contains(contains))
                AudioPool.StopLoop(keys[i], duration);
    }

    public static void StopAllLoops(float duration = 0f)
    {
        List<string> keys = new List<string>(Instance.activeLoops.Keys);

        for (int i = 0; i < keys.Count; i++)
        {
            //Debug.Log("Stopping " + keys[i]);
            AudioPool.StopLoop(keys[i], duration);
        }

       // Debug.Log("All loops stopped");
    }

    // Returns true if the current clipName is still playing.
    public static bool IsPlaying(string clipName)
    {
        return Instance.activeLoops.ContainsKey(clipName);
    }

    // Used for SFX objects to reset and re-add themselves to the pool
    public static void Reset(SFX a)
    {
        a.Set(null, 1, 1);
        a.Deactivate();
        Instance.usedPool.Remove(a);

        if (Instance.activeLoops.ContainsValue(a))
        {
            foreach (KeyValuePair<string, SFX> o in Instance.activeLoops)
            {
                if (o.Value.Equals(a))
                {
                    Instance.activeLoops.Remove(o.Key);
                    break;
                }
            }
        }

        a.gameObject.SetActive(false);
        Instance.availablePool.Add(a);
    }

    SFX GetSFX()
    {
        if (Instance.availablePool.Count < 1)
        {
            Debug.LogError("Pool is empty!"); // Log error if pool is empty
            return null;
        }
        Instance.availablePool[0].gameObject.SetActive(true); // Activate first pooled object and remove from list
        SFX p = Instance.availablePool[0];
        Instance.availablePool.RemoveAt(0);
        Instance.usedPool.Add(p);

        return p;
    }

    static void UpdateVolume(bool music, float oldV, float newV)
    {
        List<string> keys = new List<string>(Instance.activeLoops.Keys);

        for (int i = 0; i < keys.Count; i++)
        {
            if ((keys[i].ToLower() == MUSIC) == music)
                Instance.activeLoops[keys[i]].UpdateVolume(oldV, newV);
        }
    }

    static void UpdateMasterVolume(float oldV, float newV)
    {
        List<string> keys = new List<string>(Instance.activeLoops.Keys);

        for (int i = 0; i < keys.Count; i++)
        {
            float refV = (keys[i].ToLower() == MUSIC) ? musicVolume : effectsVolume;

            Instance.activeLoops[keys[i]].UpdateVolume(refV * oldV, refV * newV);
        }
    }
}