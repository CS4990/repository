using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	public float speed;
	private Rigidbody2D playerRB;
	private Vector3 change;
	private Animator anim;
    public bool[] isFull;
    public GameObject[] slots;
    public GameObject[] pickup;

    public GameObject rubyButton;
    public GameObject sapphireButton;
    public GameObject emeraldButton;

    public GameObject rubyBattleButton;
    public GameObject emeraldBattleButton;
    public GameObject sapphireBattleButton;
    public GameObject eBadge;
    public GameObject rBadge;
    public GameObject sBadge;

    public Transform sapphireLocation;
    public Transform rubyLocation;
    public Transform emeraldLocation;

    public bool emeraldBadge;
    public bool sapphireBadge;
    public bool rubyBadge;
    public bool fighting;

    /*0-Grass, 1-Water, 2-Fire, 3-Flying, 4-Fighting, 5-Rock, 6-Poison, 7-Ground*/
    void Start()
	{
		anim = GetComponent<Animator>();
		playerRB = GetComponent<Rigidbody2D>();

        AudioPool.PlayLoop(AudioHolder.instance.audioClips[Random.Range(0,2)], "MainMusic", 1,1,1);

        emeraldBadge = false;
        sapphireBadge = false;
        rubyBadge = false;
	}
	
	void Update()
	{
        if (fighting) return;

		change = Vector3.zero;
		change.x = Input.GetAxisRaw("Horizontal");
		change.y = Input.GetAxisRaw("Vertical");
		UpdateAnimationAndMove();
	}
	
	void UpdateAnimationAndMove()
	{
		if(change != Vector3.zero)
		{
			MoveCharacter();
			anim.SetFloat("moveX", change.x);
			anim.SetFloat("moveY", change.y);
			anim.SetBool("moving", true);
		}
		else
		{
			anim.SetBool("moving", false);
		}
	}

	void MoveCharacter()
	{
		playerRB.MovePosition(transform.position + change * speed * Time.deltaTime);
	}

    private void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.CompareTag("Grass"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[0], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }else if (obj.CompareTag("Water"))

        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[1], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Fire"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[2], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Flying"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[3], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Fighting"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[4], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Rock"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[5], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Poison"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[6], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Ground"))
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (isFull[i] == false)
                {
                    isFull[i] = true;
                    Instantiate(pickup[7], slots[i].transform, false);
                    //obj.gameObject.SetActive(false);
                    break;
                }
            }
        }

        if (obj.CompareTag("Emerald"))
        {
            if (isFull[0] == true && isFull[1] == true && isFull[2] == true)
            {
                emeraldButton.SetActive(true);
            }
        }

        if (obj.CompareTag("Sapphire"))
        {
            if (isFull[0] == true && isFull[1] == true && isFull[2] == true && emeraldBadge == true)
            {
                sapphireButton.SetActive(true);
            }
        }

        if (obj.CompareTag("Ruby"))
        {
            if (isFull[0] == true && isFull[1] == true && isFull[2] == true && sapphireBadge == true)
            {
                rubyButton.SetActive(true);
            }
        }

        if (obj.CompareTag("EmeraldTrainer"))
        {
            emeraldBattleButton.SetActive(true);
            emeraldBadge = true;
            FillTrainerPokemon(obj.GetComponent<NPC>());
        }

        if (obj.CompareTag("RubyTrainer"))
        {
            rubyBattleButton.SetActive(true);
            rubyBadge = true;
            FillTrainerPokemon(obj.GetComponent<NPC>());
        }

        if (obj.CompareTag("SapphireTrainer"))
        {
            sapphireBattleButton.SetActive(true);
            sapphireBadge = true;
            FillTrainerPokemon(obj.GetComponent<NPC>());
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("EmeraldTrainer"))
        {
            emeraldBattleButton.SetActive(false);
        }

        if (col.CompareTag("RubyTrainer"))
        {
            rubyBattleButton.SetActive(false);
        }

        if (col.CompareTag("SapphireTrainer"))
        {
            sapphireBattleButton.SetActive(false);
        }

        if (col.CompareTag("Sapphire") || col.CompareTag("Emerald") || col.CompareTag("Ruby"))
        {
            CancelButton();
        }
    }

    public void CancelButton()
    {
        rubyButton.SetActive(false);
        sapphireButton.SetActive(false);
        emeraldButton.SetActive(false);
    }

    void FillTrainerPokemon(NPC npc)
    {
        GameObject go = GameObject.Find("FightController");
        go.GetComponent<FightController>().botPokemon = npc.myPokemon;
    }

}