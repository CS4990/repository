﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{

    private FightController fightOptions;

    void Start()
    {
        fightOptions = GameObject.FindGameObjectWithTag("FightController").GetComponent<FightController>();
    }

    public void PlayerChoose(int choose)
    {
        fightOptions.playerChoice = choose;
        fightOptions.playersTurn = false;
    }

}
