﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Transform player;
    public GameObject tutorialScreen;
    public bool isTutorialDisplayed;

    public PlayerController PlayerController;

    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void RestartGame()
    {
        Destroy(GameObject.Find("Main Camera"));
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void EnterRubyGym()
    {
        player.transform.position = new Vector2(-75.24f, -55.66f);
        PlayerController.rubyButton.SetActive(false);
    }

    public void EnterSapphireGym()
    {
        player.transform.position = new Vector2(-94.61f, -55.88f);
        PlayerController.sapphireButton.SetActive(false);
    }

    public void EnterEmeraldGym()
    {
        player.transform.position = new Vector2 (-55.156f, -55.806f);
        PlayerController.emeraldButton.SetActive(false);
    }

    public void FightReady()
    {
        PlayerController.emeraldBattleButton.SetActive(false);
        PlayerController.rubyBattleButton.SetActive(false);
        PlayerController.sapphireBattleButton.SetActive(false);

        PlayerController.fighting = true;
    }

    public void DisplayTutorial()
    {
        if (isTutorialDisplayed == false)
        {
            tutorialScreen.SetActive(true);
            isTutorialDisplayed = true;
        }
        else
        {
            tutorialScreen.SetActive(false);
            isTutorialDisplayed = false;
        }

    }
}
