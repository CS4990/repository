﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItem : MonoBehaviour
{
    public GameObject item;

    private Transform player;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void SpawnDropped()
    {
        Vector2 playerPos = new Vector2(player.position.x, player.position.y + -1.5f);
        Instantiate(item, playerPos, Quaternion.identity);
    }
}
