﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum PType
{
    Grass,
    Water,
    Fire,
    Flying,
    Fighting,
    Rock,
    Poison,
    Ground,
}

public class FightController: MonoBehaviour
{
    public Transform player;
    public PlayerController playerObjects;
    public int playerChoice = -1;
    private int botChoice = -1;

    public Text chancesText;
    public int chances;
    public GameObject gameOverScreen;
    public GameObject gameOverScreen2;
    public GameObject quitButton;
    public GameObject resetButton;
    public GameObject continueButton;

    public bool playersTurn = true;

    public GameObject WinnerText;

    public List<Sprite> pokemonSprites;
    public GameObject botChoiceImage;

    public List<PType> botPokemon;
    Dictionary<PType, List<int>> matchups;

    private void Awake()
    {
        chances = 3;
        chancesText.text = "Chances: " + chances;

        botPokemon = new List<PType>();
        SetupMatchups();
    }

    void Update()
    {
        if (playersTurn && playerChoice == -1 || !playerObjects.fighting) return;

        BotChoose();
        CheckWinner();
        chancesText.text = "Chances: " + chances;
        CheckGameOver();
        playerChoice = -1;
        playersTurn = true;
    }

    void PlayerWon()
    {
        continueButton.SetActive(true);
        playerObjects.fighting = false;

        if (playerObjects.emeraldBadge == true)
        {
            playerObjects.eBadge.SetActive(true);
        }

        if (playerObjects.rubyBadge == true)
        {
            playerObjects.rBadge.SetActive(true);
        }

        if (playerObjects.sapphireBadge == true)
        {
            playerObjects.sBadge.SetActive(true);
        }

    }

    public void PlayerContinue()
    {
        WinnerText.SetActive(false);
        botChoiceImage.SetActive(false);
        player.transform.position = new Vector2(-1.15f, 0.06f);
        continueButton.SetActive(false);
        chances = 3;
    }

    void CheckGameOver()
    {
        if (chances == 0)
        {
            gameOverScreen.SetActive(true);
            resetButton.SetActive(true);
            quitButton.SetActive(true);
            WinnerText.SetActive(false);
            botChoiceImage.SetActive(false);
        }

        if (playerObjects.emeraldBadge == true && playerObjects.sapphireBadge == true && playerObjects.rubyBadge == true && playerObjects.fighting == false)
        {
            gameOverScreen2.SetActive(true);
            resetButton.SetActive(true);
            quitButton.SetActive(true);
            WinnerText.SetActive(false);
            botChoiceImage.SetActive(false);
        }
    }

    void CheckWinner()
    {
        if(matchups[(PType)playerChoice][botChoice] == 0)
        {
            //lose
            Debug.Log("Lost");
            WinnerText.GetComponent<Text>().text = "YOU LOST";
            chances--;
        }
        else if(matchups[(PType)playerChoice][botChoice] == 1)
        {
            //draw
            Debug.Log("Draw");
            WinnerText.GetComponent<Text>().text = "DRAW";
        }
        else //should just be 1
        {
            //win
            Debug.Log("Win");
            WinnerText.GetComponent<Text>().text = "YOU WIN";
            PlayerWon();
        }
    }

    public void PlayerChoose(int choose)
    {
        playerChoice = choose;
        playersTurn = false;
    }

    public void BotChoose()
    {
        botChoice = (int)botPokemon[Random.Range(0, 3)];
        Debug.Log("Bot Choice " + botChoice);

        botChoiceImage.SetActive(true);
        botChoiceImage.GetComponent<Image>().sprite = pokemonSprites[botChoice];
    }

    //0 lose, 1 -, 2 win
    void SetupMatchups()
    {
        matchups = new Dictionary<PType, List<int>>();

        matchups.Add(PType.Grass, new List<int>() {1,2,0,0,1,2,0,2});
        matchups.Add(PType.Water, new List<int>() {0,1,2,1,1,0,1,1});
        matchups.Add(PType.Fire, new List<int>()  {2,0,1,1,1,0,1,1});
        matchups.Add(PType.Flying, new List<int>() {2,1,1,1,2,0,1,1});
        matchups.Add(PType.Fighting, new List<int>() {1,1,1,0,1,2,0,1});
        matchups.Add(PType.Rock, new List<int>() {1,1,2,2,0,1,1,0});
        matchups.Add(PType.Poison, new List<int>() {2,1,1,1,1,0,0,0});
        matchups.Add(PType.Ground, new List<int>() {0,1,2,0,1,2,2,1});
    }
}
